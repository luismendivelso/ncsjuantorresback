package filters;

import play.filters.cors.CORSFilter;
import play.http.HttpFilters;
import play.mvc.EssentialFilter;
import javax.inject.Inject;

public class Filters implements HttpFilters {

@Inject
CORSFilter corsFilter;
public EssentialFilter[] filters() {
    return new EssentialFilter[] { corsFilter.asJava() };
}
}