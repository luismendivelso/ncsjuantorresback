package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import models.Disco;
import models.Tercero;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by JuanDavid on 07/07/2017.
 */
public class ControladorTercero extends Controller {

    public Result index() {
        return ok("NCS");
    }


    @Transactional
    public Result agregarTercero() {
        JsonNode json = request().body().asJson();
        Tercero tercero = Json.fromJson(json, Tercero.class);
        if (tercero.getCorreoElectronico() != null) {
            tercero.save();
            return ok("Exito");
        } else {
            return ok("Error");
        }
    }

    @Transactional
    public Result eliminarTercero(String correo) {
        int entity = 0;
        entity = Tercero.eliminarTercero(correo);
        if (entity != 0) {
            return ok(Json.toJson("Eliminado"));
        }
        return ok(Json.toJson("Error"));
    }

    @Transactional
    public Result actualizarTercero(String correo) {
        JsonNode json = request().body().asJson();
        int entity = 0;
        String primerNombre = json.path("valor").path("primerNombre").toString();
        String segundoNombre = json.path("valor").path("segundoNombre").toString();
        String primerApellido = json.path("valor").path("primerApellido").toString();
        String segundoApellido = json.path("valor").path("segundoApellido").toString();
        String direccion = json.path("valor").path("direccion").toString();
        String numCelular = json.path("valor").path("numCelular").toString();
        entity = Tercero.actualizarTercero(correo, primerNombre, segundoNombre, primerApellido, segundoApellido, direccion, numCelular);
        if (entity != 0) {
            return ok(Json.toJson("Eliminado"));
        }
        return ok(Json.toJson("Error"));
    }

    @Transactional
    public Result listaTerceros() {
        List tercero = Tercero.findAll(Tercero.class);
        return ok(Json.toJson(tercero));
    }

    @Transactional
    public Result listaTercero(String correoElectronico) {
        Tercero tercero = (Tercero) Tercero.findByIdString(Tercero.class, correoElectronico);
        return ok(Json.toJson(tercero));
    }


}
