package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Disco;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by JuanDavid on 08/07/2017.
 */
public class ControladorDisco extends Controller {

    @Transactional
    public Result agregarDisco() {
        JsonNode json = request().body().asJson();
        Disco disco = Json.fromJson(json, Disco.class);
        if (disco.getIdDisco() != 0) {
            disco.save();
        }else{
            return ok("Error");
        }
        return ok("Exito");
    }

    @Transactional
    public Result eliminarDisco(int idDisco){
        response().setHeader("Access-Control-Allow-Origin", "*");
        int entity = 0;
        entity = Disco.actualizarDisco(idDisco);
        if(entity != 0){
            return ok(Json.toJson("Eliminado"));
        }
        return ok(Json.toJson("Error"));
    }

    @Transactional
    public Result actualizarDisco(Long idDisco){
        response().setHeader("Access-Control-Allow-Origin", "*");

        Disco d = (Disco) Disco.findById(Disco.class, idDisco);
        if(d != null){
            JsonNode json = request().body().asJson();
            Disco disco = Json.fromJson(json, Disco.class);
            disco.update();
        }else{
            return ok("Error");
        }
        return ok("Exito");

    }

    @Transactional
    public Result listaDisco(){
        List disco =  Disco.findAll(Disco.class);
        return ok(Json.toJson(disco));
    }

    @Transactional
    public Result listaDisco(String correo){
        List disco =  Disco.findAll(Disco.class);
        return ok(Json.toJson(disco));
    }
    @Transactional
    public Result listarDiscosArtista (String correoElectronico){
        response().setHeader("Access-Control-Allow-Origin", "*");

        Object entity = null;
        try {
            entity= Disco.discosArtista(correoElectronico);
            return ok(Json.toJson(entity));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ok(Json.toJson("error"));
    }

}
