package util;

import play.db.jpa.JPA;
import javax.persistence.MappedSuperclass;
import java.util.List;

/**
 * Created by JuanDavid on 07/07/2017.
 */
@MappedSuperclass
public abstract class Entity {


    public static Object findById(Class pClass, Long id) {
        Object entity = null;
        try {
            entity = JPA.em().createQuery("FROM " + pClass.getName().toString().substring(pClass.getName().toString().lastIndexOf(".") + 1, pClass.getName().toString().length()) + " WHERE id= " + id).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public static Object findByIdString(Class pClass, String id) {
        Object entity = null;
        try {
            entity = JPA.em().createQuery("FROM " + pClass.getName().toString().substring(pClass.getName().toString().lastIndexOf(".") + 1, pClass.getName().toString().length()) + " WHERE id= " + id).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public static List findAll(Class pClass) {
        List list = null;
        try {
            return JPA.em().createQuery("FROM " + pClass.getName()).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public void save() {
        try {
            JPA.em().persist(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update() {
        try {
            JPA.em().merge(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            JPA.em().remove(JPA.em().merge(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}