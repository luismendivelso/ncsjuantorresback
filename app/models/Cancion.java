package models;

import util.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_canciones", schema = "ncs")
public class Cancion extends util.Entity implements Serializable{

    /**
     *  Clase que modela la entididad canción.
     */
    private int idMusica;
    private int idDisco;
    private int idGenero;
    private int idEstado;
    private String correoElectronico;
    private Date anioCreacion;
    private byte[] urlCancion;
    private String formatoCancion;
    private String observaciones;
    private Integer numDescargas;

    @Id
    @Column(name = "ID_MUSICA")
    public int getIdMusica() {
        return idMusica;
    }

    public void setIdMusica(int idMusica) {
        this.idMusica = idMusica;
    }

    @Basic
    @Column(name = "ID_DISCO")
    public int getIdDisco() {
        return idDisco;
    }

    public void setIdDisco(int idDisco) {
        this.idDisco = idDisco;
    }

    @Basic
    @Column(name = "ID_GENERO")
    public int getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    @Basic
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "CORREO_ELECTRONICO")
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Basic
    @Column(name = "ANIO_CREACION")
    public Date getAnioCreacion() {
        return anioCreacion;
    }

    public void setAnioCreacion(Date anioCreacion) {
        this.anioCreacion = anioCreacion;
    }

    @Basic
    @Column(name = "URL_CANCION")
    public byte[] getUrlCancion() {
        return urlCancion;
    }

    public void setUrlCancion(byte[] urlCancion) {
        this.urlCancion = urlCancion;
    }

    @Basic
    @Column(name = "FORMATO_CANCION")
    public String getFormatoCancion() {
        return formatoCancion;
    }

    public void setFormatoCancion(String formatoCancion) {
        this.formatoCancion = formatoCancion;
    }

    @Basic
    @Column(name = "OBSERVACIONES")
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Basic
    @Column(name = "NUM_DESCARGAS")
    public Integer getNumDescargas() {
        return numDescargas;
    }

    public void setNumDescargas(Integer numDescargas) {
        this.numDescargas = numDescargas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cancion cancion = (Cancion) o;

        if (idMusica != cancion.idMusica) return false;
        if (idDisco != cancion.idDisco) return false;
        if (idGenero != cancion.idGenero) return false;
        if (idEstado != cancion.idEstado) return false;
        if (correoElectronico != null ? !correoElectronico.equals(cancion.correoElectronico) : cancion.correoElectronico != null)
            return false;
        if (anioCreacion != null ? !anioCreacion.equals(cancion.anioCreacion) : cancion.anioCreacion != null)
            return false;
        if (!Arrays.equals(urlCancion, cancion.urlCancion)) return false;
        if (formatoCancion != null ? !formatoCancion.equals(cancion.formatoCancion) : cancion.formatoCancion != null)
            return false;
        if (observaciones != null ? !observaciones.equals(cancion.observaciones) : cancion.observaciones != null)
            return false;
        if (numDescargas != null ? !numDescargas.equals(cancion.numDescargas) : cancion.numDescargas != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idMusica;
        result = 31 * result + idDisco;
        result = 31 * result + idGenero;
        result = 31 * result + idEstado;
        result = 31 * result + (correoElectronico != null ? correoElectronico.hashCode() : 0);
        result = 31 * result + (anioCreacion != null ? anioCreacion.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(urlCancion);
        result = 31 * result + (formatoCancion != null ? formatoCancion.hashCode() : 0);
        result = 31 * result + (observaciones != null ? observaciones.hashCode() : 0);
        result = 31 * result + (numDescargas != null ? numDescargas.hashCode() : 0);
        return result;
    }
}
