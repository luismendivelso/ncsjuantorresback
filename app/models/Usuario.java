package models;

import org.hibernate.Criteria;
import org.hibernate.Session;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_usuarios", schema = "ncs")
public class Usuario  extends util.Entity implements Serializable {

    /**
     *  Clase que modela la entididad Usuario.
     */
    private String correoElectronico;
    private int idRol;
    private int idEstado;
    private String clave;

    @Id
    @Column(name = "CORREO_ELECTRONICO")
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Basic
    @Column(name = "ID_ROL")
    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    @Basic
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "CLAVE")
    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Usuario usuario = (Usuario) o;

        if (idRol != usuario.idRol) return false;
        if (idEstado != usuario.idEstado) return false;
        if (correoElectronico != null ? !correoElectronico.equals(usuario.correoElectronico) : usuario.correoElectronico != null)
            return false;
        if (clave != null ? !clave.equals(usuario.clave) : usuario.clave != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = correoElectronico != null ? correoElectronico.hashCode() : 0;
        result = 31 * result + idRol;
        result = 31 * result + idEstado;
        result = 31 * result + (clave != null ? clave.hashCode() : 0);
        return result;
    }

    public static List ingresarUsuario(String correoElectronico, String clave){
        List entity = null;
        Session session = (Session) JPA.em().getDelegate();
        try {
            entity = session.createSQLQuery("SELECT * FROM ncs_usuarios where CORREO_ELECTRONICO="+ correoElectronico +" and CLAVE="+ clave +"")
                    .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int actualizarUsuario(String correo, String clave){
        int entity = 0;
        Session session = (Session) JPA.em().getDelegate();
        try {
            entity = session.createSQLQuery("UPDATE ncs_usuarios SET  CLAVE = "+clave+" WHERE CORREO_ELECTRONICO = "+correo+"").executeUpdate();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
}
