name := "ncs"

version := "1.0"

lazy val `ncs` = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq( javaJdbc ,
  cache ,
  javaWs,
  filters,
  "mysql" % "mysql-connector-java" % "5.1.36",
  javaJpa,
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final")

libraryDependencies += filters

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

fork in run := true

herokuAppName in Compile := "serene-escarpment-60981"